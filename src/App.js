import { createContext } from 'react';
import { Main } from "./components/Main";

import { Context} from "./Context";

// import { useCurrentDayForecast } from './hooks/useCurrentDayForecast';

export const App = () => {
    // const { setForecast, getForecast } = useCurrentDayForecast();
    // const setForecast = () => console.log('sss');
    // const getForecast = () => console.log('sss');
    return (
        <Context.Provider>
            <Main/>
        </Context.Provider>
    );
};