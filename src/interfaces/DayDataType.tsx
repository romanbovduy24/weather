export default interface DayDataType {
    selected: boolean,
    cloudCover: number,
    humidity: number,
    temperature: number,
    weather: string,
    date: string,
    windDirection: number,
    windSpeed: number
}