export default class WeatherService {
    static getWeatherClass(weather: string) {
        switch (weather.toLocaleLowerCase()) {
            case 'drizzle':
            case 'light rain':
                return 'rainy';
            case 'cloudy':
                return 'cloudy';
            case 'sunny':
                return 'sunny';
            default:
                return '';
        }
    };
}