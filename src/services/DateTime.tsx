interface DayMap {
    [name: number]: string
}

const dayMap: DayMap = {
    0: 'Воскресенье',
    1: 'Понедельник',
    2: 'Вторник',
    3: 'Среда',
    4: 'Четверг',
    5: 'Пятница',
    6: 'Суббота',
};

const monthMap: DayMap = {
    0: 'Январь',
    1: 'Февраль',
    2: 'Март',
    3: 'Апрель',
    4: 'Май',
    5: 'Июнь',
    6: 'Июль',
    7: 'Август',
    8: 'Сентябрь',
    9: 'Октябрь',
    10: 'Ноябрь',
    11: 'Декабрь',
};

export default class DateTime {
    static getDayOfWeek(datestring: string | undefined) {
        const date = datestring ? new Date(datestring) : new Date();

        return dayMap[date.getDay()];
    };

    static getDayOfMonth(datestring: string | undefined) {
        const date = datestring ? new Date(datestring) : new Date();

        return date.getDate();
    };

    static getMonth(datestring: string | undefined) {
        const date = datestring ? new Date(datestring) : new Date();

        return monthMap[date.getMonth()];
    };
}

