export const Display = () => {
    return (
        <div className="current-weather">
            <p className="temperature">23</p>
            <p className="meta">
                <span className="rainy">%26</span>
                <span className="humidity">%60</span>
            </p>
        </div>
    );
};