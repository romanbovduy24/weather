import { Context } from '../../Context';
import { ForecastDay } from './ForecastDay';
import { ForecastItem } from './ForecastItem';

import { useFetchForecast } from '../../hooks/useFetchForecast';
import {useContext, useEffect } from "react";

export const Forecast = () => {
    const context = useContext(Context);
    const [forecast] = useFetchForecast();


    useEffect(() => {
        console.log(forecast);
    }, [forecast]);

    const items = forecast.map((data, key) => {
        return <ForecastItem key={key} {...data}/>;

    });

    return (
        <div className="forecast">
            {items}
        </div>
    );
};
