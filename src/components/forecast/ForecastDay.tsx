import { useContext } from 'react';

import DayDataType from '../../interfaces/DayDataType';

import DateTime from '../../services/DateTime';
import WeatherService from '../../services/WeatherService';

import { Context } from '../../Context';

export const ForecastDay = (props: DayDataType) => {
    const context = useContext(Context);
    console.log('ddddd');
console.log(context);
    const weatherClass = WeatherService.getWeatherClass(props.weather);
    const currentDay = DateTime.getDayOfWeek(props.date);

    return (
        <div className={`day ${weatherClass} ${props.selected ? ' selected' : ''}`} onClick={() => context ? context.setForecast(props) : console.log('s')}>
            <p>{currentDay}</p>
            <span>{props.temperature}</span>
        </div>
    );
};

