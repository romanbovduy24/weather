export const ForecastItem = (props: DayData) => {
    return (
        <div className={`day ${props.weather} ${props.selected ? ' selected' : ''}`}>
            <p>{props.dayOfWeek}</p>
            <span>{props.dayOfMonth}</span>
        </div>
    );
};

/* Types */
interface DayData {
    selected: boolean,
    weather: string,
    dayOfWeek: string,
    dayOfMonth: number
}