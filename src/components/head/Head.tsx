import { Context } from '../../Context';
import DateTime from '../../services/DateTime';
import {useContext} from "react";

export const Head = () => {
    const context = useContext(Context);

    const forecast = context.getForecast();

    const currentDay = DateTime.getDayOfWeek(forecast?.date);
    const dayOfMonth = DateTime.getDayOfMonth(forecast?.date);
    const month = DateTime.getMonth(forecast?.date);

    return (
        <div className="head">
            <div className="icon cloudy"></div>
            <div className="current-date">
                <p>{ currentDay }</p>
                <span>{ dayOfMonth } { month }</span>
            </div>
        </div>
    );
};