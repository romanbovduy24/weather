import {Filter} from "./Filter";
import {Head} from "./head/Head";
import {Display} from "./Display";
import {Forecast} from "./forecast/Forecast";

export const Main = () => {
    return (
        <main>
            <Filter/>
            <Head/>
            <Display/>
            <Forecast/>
        </main>
    );
};