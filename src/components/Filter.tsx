export const Filter = () => {
    return (
        <div className="filter">
            <span className="checkbox">Облачно</span>
            <span className="checkbox">Солнечно</span>
            <p className="custom-input">
                <label>Минимальная температура</label>
                <input id="min-temperature" type="number" value=""/>
            </p>
            <p className="custom-input">
                <label>Максимальная температура</label>
                <input id="max-temperature" type="number" value=""/>
            </p>
            <button>Отфильтровать</button>
        </div>
    );
};