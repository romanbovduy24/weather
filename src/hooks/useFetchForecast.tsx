/* Core */
import { useState, useEffect } from 'react';

import waait from "waait";

export const useFetchForecast = () => {
    const [forecast, setForecast] = useState(null);

    useEffect(() => {
        (async () => {
            const response = await fetch('https://lab.lectrum.io/rtx/api/v2/forecast?city=Kiev');
            const forecastResponse = await response.json();
            await waait(1000);

            setForecast(forecastResponse);
        })();
    }, []);

    return [forecast, setForecast];
};