/* Core */
import { useState } from 'react';

import DayDataType from '../interfaces/DayDataType';

export const useCurrentDayForecast = (): { setForecast: (forecast: DayDataType) => void, getForecast: () => DayDataType | undefined} => {
    const [dayForecast, setDayForecast] = useState<DayDataType>();

    const setForecast = (forecast: DayDataType): void => {
        setDayForecast(forecast);
    };

    const getForecast = (): DayDataType | undefined => {
        return dayForecast;
    };

    return { setForecast, getForecast };
};